resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-filesnew2021"
  acl           = "public-read"
  force_destroy = true
}
